//
//  BaseViewController.h
//  Rebtel
//
//  Created by Zowal, Rafal on 17/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface BaseViewController : UIViewController
- (BOOL)isFirstViewController;
- (UIBarButtonItem *)hamburgerMenuButton;
-(UIView *)createCustomTitleViewWithTitle:(NSString *)title;
-(UIView *)createCustomTitleViewWithTitle:(NSString *)title andIconNamed:(NSString *)iconName;
@end
