//
//  SearchViewController.m
//  Rebtel
//
//  Created by Zowal, Rafal on 17/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import "SearchViewController.h"

@interface SearchViewController ()

@end

@implementation SearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.titleView = [self createCustomTitleViewWithTitle:@"Search" andIconNamed:@"iconSearch"];
}


@end
