//
//  ProfileViewController.m
//  Rebtel
//
//  Created by Zowal, Rafal on 17/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import "ProfileViewController.h"

@interface ProfileViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *userImage;

@end

@implementation ProfileViewController

#pragma mrak - View Load Method
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self updateUIContent];
}

-(void)updateUIContent {
    self.userImage.layer.cornerRadius = 60;
    self.userImage.layer.masksToBounds = YES;
    self.userImage.layer.borderWidth = 0;
    
    self.navigationItem.titleView = [self createCustomTitleViewWithTitle:@"Profile" andIconNamed:@"IconProfile"];
}

@end
