//
//  CountryDetailViewController.h
//  Rebtel
//
//  Created by Zowal, Rafal on 15/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CountryDetailViewController : UIViewController

#pragma mark - Configure Method
-(void)configureControllerByCountryCode:(NSString *)countryCode;

@end
