//
//  CountryDetailViewController.m
//  Rebtel
//
//  Created by Zowal, Rafal on 15/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import "CountryDetailViewController.h"
#import "RestApiGetCountryDetailRequest.h"

#import "SVProgressHUD.h"

#import "Country.h"

@interface CountryDetailViewController ()

@property(nonatomic, strong) NSString *countryCode;

@property (weak, nonatomic) IBOutlet UIImageView *countryFlag;

@property (weak, nonatomic) IBOutlet UILabel *countryName;
@property (weak, nonatomic) IBOutlet UILabel *alphaCode;
@property (weak, nonatomic) IBOutlet UILabel *capital;
@property (weak, nonatomic) IBOutlet UILabel *region;
@property (weak, nonatomic) IBOutlet UILabel *subregion;
@property (weak, nonatomic) IBOutlet UILabel *population;
@property (weak, nonatomic) IBOutlet UILabel *areaSize;

@property (weak, nonatomic) IBOutlet UIStackView *contentStackView;

@end

@implementation CountryDetailViewController

#pragma mark - View Load Method
- (void)viewDidLoad {
    [super viewDidLoad];
    [[NSNotificationCenter defaultCenter]addObserver:self selector:@selector(OrientationChange:) name:UIDeviceOrientationDidChangeNotification object:nil];
    
    [self reloadData];
}

-(void)OrientationChange:(NSNotification*)notification {
    UIDeviceOrientation Orientation=[[UIDevice currentDevice]orientation];
    
    if(Orientation==UIDeviceOrientationLandscapeLeft || Orientation==UIDeviceOrientationLandscapeRight) {
        [self.contentStackView setAxis:UILayoutConstraintAxisHorizontal];
    } else if(Orientation==UIDeviceOrientationPortrait) {
        [self.contentStackView setAxis:UILayoutConstraintAxisVertical];
    }
}

-(void)updateUIContetnt:(Country *)country {
    self.countryName.text = country.name;
    self.alphaCode.text = country.alpha2Code;
    self.capital.text = country.capital;
    self.region.text = country.region;
    self.subregion.text = country.subregion;
    self.population.text = [country.population stringValue];
    self.areaSize.text = [country.area stringValue];
    
    NSString *imagePath = [NSString stringWithFormat:@"Flags.bundle/png250px/%@", [country.alpha2Code lowercaseString]];
    UIImage *flag = [UIImage imageNamed:imagePath];
    
    if(flag) {
        self.countryFlag.image = flag;
    } else {
        self.countryFlag.image = [UIImage imageNamed:@"questionMark"];
    }
}

-(void)reloadData {
    NSDictionary *parameters = @{@"codes" : self.countryCode};
    RestApiGetCountryDetailRequest *request = [[RestApiGetCountryDetailRequest alloc] initWithParameters:parameters];
    
    [SVProgressHUD show];
    [request startWithSuccessBlock:^(id respodObject) {
        [SVProgressHUD dismiss];
        if(respodObject) {
            [self updateUIContetnt:respodObject];
        }
        
    } andFailureBlock:^(NSError *error) {
        NSLog(@"Error on RestApiGetCountryDetailRequest");
        [SVProgressHUD dismiss];
    }];
}


#pragma mark - Configure Method
-(void)configureControllerByCountryCode:(NSString *)countryCode {
    self.countryCode = countryCode;
}

@end
