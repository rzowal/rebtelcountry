//
//  SettingViewController.m
//  Rebtel
//
//  Created by Zowal, Rafal on 17/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import "SettingsViewController.h"

@interface SettingsViewController ()

@end

@implementation SettingsViewController

#pragma mrak - View Load Method
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.titleView = [self createCustomTitleViewWithTitle:@"Settings" andIconNamed:@"IconSettings"];
}


@end
