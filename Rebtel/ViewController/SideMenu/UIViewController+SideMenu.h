//
//  UIViewController+SideMenu.h
//  Rebtel
//
//  Created by Zowal, Rafal on 13/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "SideMenuRootViewController.h"

@interface UIViewController(SideMenu)

@property (strong, readonly, nonatomic) SideMenuRootViewController *menuViewController;

@end
