//
//  SideMenuRootViewController.h
//  Rebtel
//
//  Created by Zowal, Rafal on 13/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import "RESideMenu.h"

@interface SideMenuRootViewController : RESideMenu <RESideMenuDelegate>

-(void)showStory:(NSString *)identyfier;
-(void)showView:(NSString *)viewName storyboard:(NSString *)storyboardName;


@end
