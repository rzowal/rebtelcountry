//
//  SideMenuViewController.m
//  Rebtel
//
//  Created by Zowal, Rafal on 13/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import "SideMenuViewController.h"
#import "SideMenuTableViewCell.h"

#import "Masonry.h"

#import "AppUIGlobal.h"

@interface SideMenuViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSMutableArray *menuItems;

@end

@implementation SideMenuViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.menuItems = [NSMutableArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"MenuItems" ofType:@"plist"]];
}

#pragma mark - UITableView Datasource
- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SideMenuTableViewCell *cell = (SideMenuTableViewCell *)[tableView dequeueReusableCellWithIdentifier:@"SideMenuTableViewCell"];
    [cell configureCellWithTitle:self.menuItems[indexPath.row][@"Title"] icon:self.menuItems[indexPath.row][@"Icons"] andNumber:self.menuItems[indexPath.row][@"Number"]];
    
    return cell;
}

- (void)tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath {
    [((SideMenuTableViewCell *)cell).numberLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.leading.mas_equalTo(self.view.frame.size.width / 3 * 2);
    }];
    
    [cell layoutIfNeeded];
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.menuItems.count;
}

#pragma mark - UITableView Delegate
- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self.tableView deselectRowAtIndexPath:indexPath animated:true];
    NSDictionary *dict = self.menuItems[indexPath.row];
    
    NSString *identifier = dict[@"Identyfier"];
    
    [self.menuViewController showStory:identifier];
}

@end

