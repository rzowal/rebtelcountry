//
//  SideMenuViewController.h
//  Rebtel
//
//  Created by Zowal, Rafal on 13/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UIViewController+SideMenu.h"

@interface SideMenuViewController : UIViewController <RESideMenuDelegate>

@end
