//
//  SideMenuTableViewCell.h
//  Rebtel
//
//  Created by Zowal, Rafal on 13/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SideMenuTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *numberLabel;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingConst;

- (void)configureCellWithTitle:(NSString *)title icon:(NSString *)icon andNumber:(NSNumber *)number;
- (void)configureCellWithTitle:(NSString *)title andNumber:(NSNumber *)number;

@end
