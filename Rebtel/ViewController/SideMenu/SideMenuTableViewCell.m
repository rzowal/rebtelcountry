//
//  SideMenuTableViewCell.m
//  Rebtel
//
//  Created by Zowal, Rafal on 13/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import "SideMenuTableViewCell.h"

#import "AppUIGlobal.h"
#import "Masonry.h"

@interface SideMenuTableViewCell ()
@property (weak, nonatomic) IBOutlet UIImageView *menuIconImage;
@property (weak, nonatomic) IBOutlet UILabel *menuTitleLabel;
@property (strong, nonatomic) NSString *title;
@end

@implementation SideMenuTableViewCell

- (void)awakeFromNib {
    [super awakeFromNib];
}

- (void)configureCellWithTitle:(NSString *)title icon:(NSString *)icon andNumber:(NSNumber *)number {
    self.menuTitleLabel.text = title;
    self.menuTitleLabel.textColor = WHITE;
    self.menuTitleLabel.font = FONT_DEFAULT(22);
    
    if ([title containsString:@"Saved"]) {
        title = @"savedJobs";
    }
    
    self.menuIconImage.image = [UIImage imageNamed:icon];
    float labelHeight = self.menuTitleLabel.font.pointSize;
    
    [self.menuTitleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(labelHeight+5);
    }];
    
    
    [self.numberLabel mas_remakeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(32);
    }];
    
    self.numberLabel.hidden = true;
    
    if ([number intValue] != 0) {
        self.numberLabel.hidden = false;
        self.numberLabel.text = [number description];
        self.numberLabel.layer.cornerRadius = self.numberLabel.frame.size.height / 2;
        self.numberLabel.layer.masksToBounds = YES;
        self.numberLabel.layer.borderWidth = 0;
        self.numberLabel.font = FONT_BOLD_DEFAULT(14);
    }
}

- (void)configureCellWithTitle:(NSString *)title andNumber:(NSNumber *)number
{
    [self configureCellWithTitle:title icon:nil andNumber:number];
    
    self.menuIconImage.image = [UIImage imageNamed:[NSString stringWithFormat:@"%@Icon", [title lowercaseString]]];
}

- (void)setHighlighted:(BOOL)highlighted animated:(BOOL)animated {
    self.contentView.backgroundColor = highlighted ? [UIColor lightGrayColor] : [UIColor clearColor];
}


@end
