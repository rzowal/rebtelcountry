//
//  SideMenuRootViewController.m
//  Rebtel
//
//  Created by Zowal, Rafal on 13/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import "SideMenuRootViewController.h"
#import "SideMenuViewController.h"

#import "AppUIGlobal.h"

@interface SideMenuRootViewController ()

@property(nonatomic, strong) NSArray *storyItems;

@end

@implementation SideMenuRootViewController

- (void)awakeFromNib {
    self.storyItems = [NSArray arrayWithContentsOfFile:[[NSBundle mainBundle] pathForResource:@"MenuItems" ofType:@"plist"]];
    
    self.leftMenuViewController = [self.storyboard instantiateViewControllerWithIdentifier:@"sideMenuViewController"];
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Home" bundle:nil];
    self.contentViewController = [storyboard instantiateViewControllerWithIdentifier:@"HomeNav"];
    
    self.menuPreferredStatusBarStyle = UIStatusBarStyleLightContent;
    self.contentViewShadowEnabled = YES;
    
    self.scaleMenuView = YES;
    self.backgroundImage = [UIImage imageNamed:@"menuBackround"];
    
    self.delegate = (SideMenuViewController *)self.leftMenuViewController;
}

-(void)showStory:(NSString *)identyfier {
    NSArray *filteredList = [self.storyItems filteredArrayUsingPredicate:[NSPredicate predicateWithFormat:@"Identyfier = %@",identyfier]];
    if(filteredList.count) {
        NSDictionary *dict = [filteredList firstObject];
        [self showView:dict[@"ViewController"] storyboard:dict[@"Storyboard"]];
    }
}

-(void)showView:(NSString *)viewName storyboard:(NSString *)storyboardName {
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:storyboardName bundle:nil];
    [self setContentViewController:[storyboard instantiateViewControllerWithIdentifier:viewName] animated:YES];
    [self hideMenuViewController];
}


@end
