//
//  UIViewController+SideMenu.m
//  Rebtel
//
//  Created by Zowal, Rafal on 13/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import "UIViewController+SideMenu.h"

@implementation UIViewController(SideMenu)

- (SideMenuRootViewController *)menuViewController {
    UIViewController *iter = self.parentViewController;
    while (iter) {
        if ([iter isKindOfClass:[SideMenuRootViewController class]]) {
            return (SideMenuRootViewController *)iter;
        } else if (iter.parentViewController && iter.parentViewController != iter) {
            iter = iter.parentViewController;
        } else {
            iter = nil;
        }
    }
    return nil;
}

@end
