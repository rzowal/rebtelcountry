//
//  HomeViewController.m
//  Rebtel
//
//  Created by Zowal, Rafal on 17/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import "HomeViewController.h"

@interface HomeViewController ()

@end

@implementation HomeViewController

#pragma mrak - View Load Method
- (void)viewDidLoad {
    [super viewDidLoad];
    
    self.navigationItem.titleView = [self createCustomTitleViewWithTitle:@"Home" andIconNamed:@"IconHome"];
}


@end
