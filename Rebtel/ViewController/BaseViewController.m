//
//  BaseViewController.m
//  Rebtel
//
//  Created by Zowal, Rafal on 17/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import "BaseViewController.h"
#import "CustomTitleViewController.h"
#import "RESideMenu.h"

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    if([self isFirstViewController]) {
        self.navigationItem.leftBarButtonItem = [self hamburgerMenuButton];
    }
}

- (BOOL)isFirstViewController {
    return (self.navigationController.viewControllers.firstObject == self);
}

- (UIBarButtonItem *)hamburgerMenuButton {
    return [[UIBarButtonItem alloc] initWithImage:[UIImage imageNamed:@"nav_menu"] style:UIBarButtonItemStylePlain target:self.sideMenuViewController action:@selector(presentLeftMenuViewController)];
}

-(UIView *)createCustomTitleViewWithTitle:(NSString *)title
{
    CustomTitleViewController* titleView = [[CustomTitleViewController alloc] initWithNibName: @"CustomTitleViewController" bundle: nil];
    [titleView configureCustomTitle:title];
    return titleView.view;
}

-(UIView *)createCustomTitleViewWithTitle:(NSString *)title andIconNamed:(NSString *)iconName
{
    CustomTitleViewController* titleView = [[CustomTitleViewController alloc] initWithNibName: @"CustomTitleViewController" bundle: nil];
    [titleView configureCustomIconName:iconName];
    [titleView configureCustomTitle:title];
    return titleView.view;
}
@end
