//
//  HomeViewController.m
//  FlightRadar24
//
//  Created by Zowal, Rafal on 10/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import "CountryViewController.h"
#import "CountryDetailViewController.h"

#import "Country.h"
#import "CountryCell.h"
#import "SVProgressHUD.h"
#import "RestApiGetAllCountriesReqeust.h"

#define countryDefineSegueIdentifier @"CountryDetailSegueIdentifier"

@interface CountryViewController () <UITableViewDelegate, UITableViewDataSource>

@property (nonatomic, weak) IBOutlet UITableView *tableView;

@property (nonatomic, strong) NSArray *countryModels;

@end

#pragma mark - Inicialization View Method
@implementation CountryViewController

- (void)viewDidLoad {
    [super viewDidLoad];

    self.tableView.estimatedRowHeight = 80.0;
    self.tableView.rowHeight = UITableViewAutomaticDimension;
    
    self.navigationItem.titleView = [self createCustomTitleViewWithTitle:@"Country List" andIconNamed:@"iconCountry"];
    
    [self reloadData];
}

-(void)reloadData {
    RestApiGetAllCountriesReqeust *request = [[RestApiGetAllCountriesReqeust alloc] init];
    
    [SVProgressHUD show];
    [request startWithSuccessBlock:^(id respodObject) {
        self.countryModels = respodObject;
        [self.tableView reloadData];
        [SVProgressHUD dismiss];
    } andFailureBlock:^(NSError *error) {
        NSLog(@"Error on RestApiGetAllCountriesReqeust");
        [SVProgressHUD dismiss];
    }];
}

#pragma mark - UITableViewDataSource Method
-(NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {
    return self.countryModels.count;
}

-(UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    static NSString *cellIndentifier = @"CountryCellIdentifier";
    
    CountryCell *countryCell = [tableView dequeueReusableCellWithIdentifier:cellIndentifier];
    [countryCell configureCellByCountryModel:[self.countryModels objectAtIndex:indexPath.row]];
    
    return countryCell;
}

#pragma mark - UITableViewDelegate Method
-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath {
    [self performSegueWithIdentifier:countryDefineSegueIdentifier sender:[self.countryModels objectAtIndex:indexPath.row]];
}

#pragma mark - Navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([segue.identifier isEqualToString:countryDefineSegueIdentifier]) {
        CountryDetailViewController *controller = segue.destinationViewController;
        [controller configureControllerByCountryCode:[(Country *)sender alpha2Code]];
    }
}


@end
