//
//  HomeCell.h
//  FlightRadar24
//
//  Created by Zowal, Rafal on 10/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import <UIKit/UIKit.h>

@class Country;

@interface CountryCell : UITableViewCell

#pragma mark - Configure Cell Method
-(void)configureCellByCountryModel:(Country *)countryModel;

@end
