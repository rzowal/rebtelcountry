//
//  HomeCell.m
//  FlightRadar24
//
//  Created by Zowal, Rafal on 10/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import "CountryCell.h"

#import "Country.h"

@interface CountryCell ()

@property (nonatomic, weak) IBOutlet UILabel *countryName;
@property (nonatomic, weak) IBOutlet UIImageView *flag;

@end

@implementation CountryCell

- (void)awakeFromNib {
    [super awakeFromNib];
    // Initialization code
}

#pragma mark - Configure Cell Method
-(void)configureCellByCountryModel:(Country *)countryModel {
    self.countryName.text = countryModel.name;
    NSString *imagePath = [NSString stringWithFormat:@"Flags.bundle/png250px/%@", [countryModel.alpha2Code lowercaseString]];
    UIImage *flag = [UIImage imageNamed:imagePath];
    
    if(flag) {
        self.flag.image = flag;
    } else {
        self.flag.image = [UIImage imageNamed:@"questionMark"];
    }
}

@end
