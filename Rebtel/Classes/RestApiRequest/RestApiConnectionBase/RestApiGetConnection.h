//
//  RestApiConnection.h
//  FlightRadar24
//
//  Created by Zowal, Rafal on 10/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RebtelConst.h"

@class RestApiBuilder;

@interface RestApiGetConnection : NSObject <NSURLConnectionDelegate> {
@protected
    NSMutableURLRequest *request_;
    successBlock successBlock_;
    failureBlock failureBlock_;
    
    RestApiBuilder *_builder;
}

@property (nonatomic, readonly) NSMutableURLRequest *request;

@property (nonatomic, copy, readonly) successBlock successBlock;
@property (nonatomic, copy, readonly) failureBlock failureBlock;

-(instancetype)initWithParameters:(NSDictionary *)parameters;

#pragma mark - request Method
- (void)startWithSuccessBlock:(void(^)(id respodObject))succeccBlock andFailureBlock:(void(^)(NSError *error))failureBlock;

@end

//We can create here mutch more classes - like DELETE, PUT, or even separate it by serialization POST JSON, POST NSDATA, ect...
@interface RestApiPostConnection : RestApiGetConnection

@end

@interface RestApiPostJsonConnection : RestApiGetConnection

@end
