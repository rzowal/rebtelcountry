//
//  RestApiConnection.m
//  FlightRadar24
//
//  Created by Zowal, Rafal on 10/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import "RestApiGetConnection.h"
#import "RestApiBuilder.h"
#import "RequestDirector.h"

#import "RebtelBuilder.h"

@implementation RestApiGetConnection

@synthesize request = request_, successBlock = successBlock_, failureBlock = failureBlock_;

- (instancetype)init {
    self = [super init];
    if(self) {
        _builder = [[RebtelBuilder alloc] init];
        [self createBasicRestApi:_builder];
    
        request_ = [_builder request];
    }
    
    return self;
}

-(instancetype)initWithParameters:(NSDictionary *)parameters {
    self = [self init];
    
    [self configureParamters:parameters andBuilder:_builder];
    
    return self;
}

-(NSString *)pathForRequest {
    return @"";
}

// Right now we don't have time to creating different posibilities about respond serialization, currently for this project we can assume (that was correct) all was responded by JSON.
#pragma mark - request Method
-(void)startWithSuccessBlock:(void (^)(id))succeccBlock andFailureBlock:(void (^)(NSError *))failureBlock {
    successBlock_ = succeccBlock;
    failureBlock_ = failureBlock;
    
    NSURLSessionConfiguration *config = [NSURLSessionConfiguration defaultSessionConfiguration];
    NSURLSession *session = [NSURLSession sessionWithConfiguration:config];
    
    // NSURLSessionDataTask is mostly used for GET Method - but it also work. We can use also NSURLSessionUploadTask for POST method but i want to have
    // body parametry in setHTTPBody not in session method.
    NSURLSessionDataTask *dataTask = [session dataTaskWithRequest:request_ completionHandler:^(NSData * _Nullable data, NSURLResponse * _Nullable response, NSError * _Nullable error) {
        
        NSError *jsonError = nil;
        NSArray* jsonRespond;
        if(data) {
            jsonRespond = [NSJSONSerialization JSONObjectWithData:data options:0 error:&jsonError];
        }
#pragma warrning ADDED LOGER For NSError on response
        if(!jsonError && self.successBlock) {
            dispatch_async(dispatch_get_main_queue(), ^{
                successBlock_([self processResponse:jsonRespond]);
            });
        } else if(self.failureBlock) {
            dispatch_async(dispatch_get_main_queue(), ^{
                self.failureBlock(jsonError);
            });
        }
    }];
    
    [dataTask resume];
}

#pragma mark - Response Method
-(id)processResponse:(id)responseObj {
    return responseObj;
}

#pragma mark - Other
-(void)createBasicRestApi:(RestApiBuilder *)builder {
    RequestDirector *director = [[RequestDirector alloc] initWithBuilder:builder];
    [builder setPATH:[self pathForRequest]];
    
    [director createGetRequestWith:builder];
}

-(void)configureParamters:(NSDictionary *)parameters andBuilder:(RestApiBuilder *)builder {
    if (parameters) {
        [builder configureParamaters:parameters];
        [builder result];
        request_ = [_builder request];
    }
}


@end

@implementation RestApiPostConnection

-(void)createBasicRestApi:(RestApiBuilder *)builder {
    RequestDirector *director = [[RequestDirector alloc] initWithBuilder:builder];
    [builder setPATH:[self pathForRequest]];
    
    [director createPostRequestWith:builder];
}

@end

@implementation RestApiPostJsonConnection

-(void)createBasicRestApi:(RestApiBuilder *)builder {
    RequestDirector *director = [[RequestDirector alloc] initWithBuilder:builder];
    [builder setPATH:[self pathForRequest]];
    
    [director createPostJSonRequestWith:builder];
}

@end
