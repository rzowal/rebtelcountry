//
//  RestApiGetCountryDetailRequest.h
//  Rebtel
//
//  Created by Zowal, Rafal on 16/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import "RestApiGetConnection.h"

@interface RestApiGetCountryDetailRequest : RestApiGetConnection

@end
