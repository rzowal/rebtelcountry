//
//  RestApiGetAllCountriesReqeust.m
//  Rebtel
//
//  Created by Zowal, Rafal on 10/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import "RestApiGetAllCountriesReqeust.h"

#import "Country.h"

@implementation RestApiGetAllCountriesReqeust

-(NSString *)pathForRequest {
    return @"all";
}

#pragma mark - Response Method
-(id)processResponse:(id)responseObj {
    NSMutableArray *resultsList = [NSMutableArray new];
    
    for (NSDictionary *parametersList in responseObj) {
        Country *country = [[Country alloc] initWithDictionary:parametersList error:nil];
        
        [resultsList addObject:country];
    }

    return resultsList;
}

@end
