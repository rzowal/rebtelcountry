//
//  RestApiGetCountryDetailRequest.m
//  Rebtel
//
//  Created by Zowal, Rafal on 16/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import "RestApiGetCountryDetailRequest.h"

#import "Country.h"

@implementation RestApiGetCountryDetailRequest

-(NSString *)pathForRequest {
    return @"alpha";
}

#pragma mark - Response Method
-(id)processResponse:(id)responseObj {

    Country *country = [[Country alloc] initWithDictionary:[responseObj firstObject] error:nil];
    return country;
}

@end
