//
//  FlightRadarBuilder.m
//  FlightRadar24
//
//  Created by Zowal, Rafal on 10/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import "RebtelBuilder.h"

@implementation RebtelBuilder

#pragma mark - Configure Method
-(void)configureUrlPath {
    [super configureUrlPath];
    self.APIURL = @"https://restcountries.eu/";
    self.SITEURL = @"rest/";
    self.WS_API_URL_PATH_PREFIX = @"v1/";
}

#pragma mark - Result Method (Builder)
-(RestApiBuilder *)result {
    [super result];
    
    // You can place here anything what will be needed only for Rebtel Api Request (restcountries)
    
    return self;
}

@end
