//
//  RestApiBuilder.m
//  FlightRadar24
//
//  Created by Zowal, Rafal on 10/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import "RestApiBuilder.h"
#import "Utils.h"

@interface RestApiBuilder ()

@end

@implementation RestApiBuilder

@synthesize request = request_, requestParameters = requestParameters_;

-(instancetype)init {
    self = [super init];
    if(self) {
        requestParameters_ = [NSMutableDictionary new];
        [self configureUrlPath];
    }
    
    return self;
}

-(void)configureUrlPath {
    self.APIURL = @"";
    self.SITEURL = @"";
    self.WS_API_URL_PATH_PREFIX = @"";
}

#pragma mark - Builder Method (REQUEST)
-(RestApiBuilder *)buildNewPostRequest {
    request_ = [NSMutableURLRequest requestWithURL:[self buildNSURL]];
    [request_ setHTTPMethod:@"POST"];
    
    return self;
}

-(RestApiBuilder *)configureContentType:(ContentType)contentType {
    [request_ setValue:[self contentTypeFormat:contentType] forHTTPHeaderField:@"Content-Type"];
    requestContentType_ = contentType;
    
    return self;
}

-(RestApiBuilder *)buildNewGetRequest {
    request_ = [NSMutableURLRequest requestWithURL:[self buildNSURL]];
    [request_ setHTTPMethod:@"GET"];
    
    return self;
}

-(RestApiBuilder *)addNewParamater:(NSString *)key andValue:(NSString *)value {
    requestParameters_[key] = value;
    
    return self;
}

-(RestApiBuilder *)configureParamaters:(NSDictionary *)parameters {
    requestParameters_ = [[NSMutableDictionary alloc] initWithDictionary:parameters];
    
    return self;
}

-(RestApiBuilder *)result {
    if(self.requestParameters) {
        switch (requestContentType_) {
            case ContentTypeXWwwFormUrlencoded:
                [self buildxWwwFormParamsSerialization];
                break;
            case ContentTypeJSon:
                [self buildJsoneParamsSerialization];
                break;
                
            default:
                break;
        }
    }
    
    return self;
}

#pragma mark - Path ConfigureMethod
-(RestApiBuilder *)configurePathMethod:(NSString *)path {
    self.PATH = path;
    [request_ setURL:[self buildNSURL]];
    
    return self;
}

#pragma mark - requestSerialization
-(void)buildJsoneParamsSerialization {
    NSData *jsonData = [NSJSONSerialization dataWithJSONObject:self.requestParameters options:0 error:nil];
    
    // Checking the format
    NSString *urlString =  [[NSString alloc] initWithData:jsonData encoding:NSUTF8StringEncoding];
    
    // Convert your data and set your request's HTTPBody property
    NSString *stringData = [[NSString alloc] initWithFormat:@"jsonRequest=%@", urlString];
    
    NSData *requestBodyData = [stringData dataUsingEncoding:NSUTF8StringEncoding];
    
    self.request.HTTPBody = requestBodyData;
}

-(void)buildxWwwFormParamsSerialization {
    NSString *params = [Utils makeParamtersString:self.requestParameters withEncoding:NSUTF8StringEncoding];
    if ([request_.HTTPMethod isEqualToString:@"GET"] && params) {
        NSString *absoultePath = request_.URL.absoluteString;
        request_.URL = [NSURL URLWithString:[NSString stringWithFormat:@"%@?%@",absoultePath,params]];
    } else {
        NSData *body = [params dataUsingEncoding:NSUTF8StringEncoding];
    
        self.request.HTTPBody = body;
    }
}

#pragma mark - Other Method
-(NSURL *)buildNSURL {
    return [NSURL URLWithString:[NSString stringWithFormat:@"%@%@%@%@", self.APIURL, self.SITEURL, self.WS_API_URL_PATH_PREFIX, self.PATH]];
}

// This method can be removed to Food2ForkBuilder method - for example we don't have to build content type here, we can just ovveride method on couple clases who will be responsibilities for creatin POS GET... method (example: Food2ForkJSONEPOST, Food2ForXWWWFormPost... )
-(NSString *)contentTypeFormat:(ContentType)contentType {
    NSString *contentFormat = @"";
    
    switch (contentType) {
        case ContentTypeXWwwFormUrlencoded:
            contentFormat = @"application/x-www-form-urlencoded; charset=utf-8";
            break;
            
        case ContentTypeJSon:
            contentFormat = @"application/json";
            break;
            
        default:
            contentFormat = @"application/x-www-form-urlencoded; charset=utf-8";
            break;
    }
    
    return contentFormat;
}

@end
