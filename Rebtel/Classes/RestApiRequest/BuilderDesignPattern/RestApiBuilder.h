//
//  RestApiBuilder.h
//  FlightRadar24
//
//  Created by Zowal, Rafal on 10/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "RebtelConst.h"

@interface RestApiBuilder : NSObject {
@protected
    NSMutableURLRequest *request_;
    NSMutableDictionary *requestParameters_;
    
    ContentType requestContentType_;
}

@property (nonatomic, readonly) NSMutableDictionary *requestParameters;
@property (nonatomic, readonly) NSMutableURLRequest *request;

@property (nonatomic, strong) NSString *APIURL;
@property (nonatomic, strong) NSString *SITEURL;
@property (nonatomic, strong) NSString *WS_API_URL_PATH_PREFIX;

@property (nonatomic, strong) NSString *PATH;

#pragma mark - Configure Method
-(void)configureUrlPath;

#pragma mark - Builder Method (REQUEST)
-(RestApiBuilder *)buildNewPostRequest;
-(RestApiBuilder *)buildNewGetRequest;

-(RestApiBuilder *)configureContentType:(ContentType)contentType;

-(RestApiBuilder *)addNewParamater:(NSString *)key andValue:(NSString *)value;
-(RestApiBuilder *)configureParamaters:(NSDictionary *)parameters;

-(RestApiBuilder *)result;

#pragma mark - Path ConfigureMethod
-(RestApiBuilder *)configurePathMethod:(NSString *)path;

@end
