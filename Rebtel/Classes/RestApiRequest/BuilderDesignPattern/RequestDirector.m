//
//  RequestDirector.m
//  FlightRadar24
//
//  Created by Zowal, Rafal on 10/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import "RequestDirector.h"
#import "RestApiBuilder.h"

@interface RequestDirector ()

@property (nonatomic, weak) RestApiBuilder *builder;

@end

@implementation RequestDirector

#pragma mark - initialize mehod
-(id)initWithBuilder:(RestApiBuilder *)concreteBuilder {
    if (self = [super init]) {
        self.builder = concreteBuilder;
    }
    
    return self;
}

-(void)configureBuilder:(RestApiBuilder *)concreteBuilder {
    if (self) {
        self.builder = concreteBuilder;
    }
}

// Inside this method we will put mutch more builder method like serialization Request, Respond Queue ect...
#pragma mark - Create Method
-(RestApiBuilder *)createPostRequestWith:(RestApiBuilder *)builder {
    [[[builder buildNewPostRequest]
      configureContentType:ContentTypeXWwwFormUrlencoded]
     result];
    
    return builder;
}

-(RestApiBuilder *)createPostJSonRequestWith:(RestApiBuilder *)builder {
    [[[builder buildNewPostRequest]
      configureContentType:ContentTypeJSon]
     result];
    
    return builder;
}

// There we must create different solution because GET (parameters) was cointained in URL
// for no time we can not do that right now
-(RestApiBuilder *)createGetRequestWith:(RestApiBuilder *)builder {
    [[[builder buildNewGetRequest]
      configureContentType:ContentTypeXWwwFormUrlencoded]
     result];
    
    return builder;
}


// There we can craete mutch more option like DELETE , PUT + Encoding variation


@end
