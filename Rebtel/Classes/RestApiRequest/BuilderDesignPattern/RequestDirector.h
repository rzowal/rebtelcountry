//
//  RequestDirector.h
//  FlightRadar24
//
//  Created by Zowal, Rafal on 10/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import <Foundation/Foundation.h>

@class RestApiBuilder;

@interface RequestDirector : NSObject

#pragma mark - initialize mehod
-(id)initWithBuilder:(RestApiBuilder *)concreteBuilder;
-(void)configureBuilder:(RestApiBuilder *)concreteBuilder;


#pragma mark - Create Method
-(RestApiBuilder *)createPostRequestWith:(RestApiBuilder *)builder;
-(RestApiBuilder *)createPostJSonRequestWith:(RestApiBuilder *)builder;
-(RestApiBuilder *)createGetRequestWith:(RestApiBuilder *)builder;

// There we can craete mutch more option like DELETE , PUT + Encoding variation
@end
