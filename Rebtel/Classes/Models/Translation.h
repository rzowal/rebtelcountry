//
//  Translation.h
//  Rebtel
//
//  Created by Zowal, Rafal on 13/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import "JSONModel.h"

@protocol Translation <NSObject>
@end

@interface Translation : JSONModel

@property (nonatomic, strong) NSString *de;
@property (nonatomic, strong) NSString *es;
@property (nonatomic, strong) NSString *fr;
@property (nonatomic, strong) NSString *ja;
@property (nonatomic, strong) NSString *it;

@end
