//
//  Country.h
//  Rebtel
//
//  Created by Zowal, Rafal on 13/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import "JSONModel.h"

#import "Translation.h"

@interface Country : JSONModel

@property (nonatomic, strong) NSString *name;

@property (nonatomic, strong) NSArray *topLevelDomain;

@property (nonatomic, strong) NSString *alpha2Code;
@property (nonatomic, strong) NSString *alpha3Code;

@property (nonatomic, strong) NSArray *callingCodes;
@property (nonatomic, strong) NSString *capital;

@property (nonatomic, strong) NSArray *altSpellings;
@property (nonatomic, strong) NSNumber *relevance;
@property (nonatomic, strong) NSString *region;
@property (nonatomic, strong) NSString *subregion;

@property (nonatomic, strong) Translation *translations;
@property (nonatomic, strong) NSNumber *population;
@property (nonatomic, strong) NSArray *latlng;
@property (nonatomic, strong) NSString *demonym;

@property (nonatomic, strong) NSNumber *area;
@property (nonatomic, strong) NSNumber *gini;
@property (nonatomic, strong) NSArray *timezones;

@property (nonatomic, strong) NSArray *borders;

@property (nonatomic, strong) NSString *nativeName;
@property (nonatomic, strong) NSString *numericCode;
@property (nonatomic, strong) NSArray *currencies;
@property (nonatomic, strong) NSArray *languages;

@end
