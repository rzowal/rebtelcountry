//
//  RebtelConst.h
//  Rebtel
//
//  Created by Zowal, Rafal on 10/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#ifndef RebtelConst_h
#define RebtelConst_h

// Typedef definicion
typedef void (^successBlock)(id responseObject);
typedef void (^failureBlock)(NSError *error);

// Enum Definicion
typedef NS_ENUM(NSUInteger, ContentType) {
    ContentTypeXWwwFormUrlencoded,
    ContentTypeJSon
};

#endif /* RebtelConst_h */
