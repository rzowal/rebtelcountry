//
//  AppUIGlobal.h
//  Rebtel
//
//  Created by Zowal, Rafal on 13/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#ifndef AppUIGlobal_h
#define AppUIGlobal_h

#define FONT_DEFAULT(_size_)                        [UIFont fontWithName:@"Lato-Regular" size:(_size_)]
#define FONT_BOLD_DEFAULT(_size_)                   [UIFont fontWithName:@"Lato-Bold" size:(_size_)]

#define DARK_BLUE                                   [UIColor colorWithRed:50/255.0f green:59/255.0f blue:67/255.0f alpha:1.0]
#define WHITE                                       [UIColor colorWithRed:255/255.0f green:255/255.0f blue:255/255.0f alpha:1.0]

#define NAVIGATION_BAR_COLOR                        [UIColor colorWithRed:134/255.0f green:50/255.0f blue:66/255.0f alpha:1.0]
#define NAVIGATION_BAR_TINT_COLOR                   [UIColor colorWithRed:234/255.0f green:50/255.0f blue:66/255.0f alpha:1.0]

#endif /* AppUIGlobal_h */
