//
//  Utils.h
//  FlightRadar24
//
//  Created by Zowal, Rafal on 10/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Utils : NSObject

#pragma mark - Request Parser / Encoding
+(NSString*)makeParamtersString:(NSDictionary*)parameters withEncoding:(NSStringEncoding)encoding;

@end
