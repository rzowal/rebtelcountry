//
//  CustomTitleViewController.h
//  Rebtel
//
//  Created by Zowal, Rafal on 17/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CustomTitleViewController : UIViewController

-(void)configureCustomIconName:(NSString *)name;
-(void)configureCustomTitle:(NSString *)title;

@end
