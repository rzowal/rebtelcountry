//
//  CustomTitleViewController.m
//  Rebtel
//
//  Created by Zowal, Rafal on 17/10/2016.
//  Copyright © 2016 Zowal, Rafal. All rights reserved.
//

#import "CustomTitleViewController.h"

#import "Masonry.h"
#import "AppUIGlobal.h"

@interface CustomTitleViewController ()
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;
@property (weak, nonatomic) IBOutlet UIImageView *titleImage;
@property (strong, nonatomic) NSString *customTitle;
@property (strong, nonatomic) NSString *customIconName;
@end

@implementation CustomTitleViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.titleLabel.text = [self.customTitle capitalizedString];
    
    if ([self.customTitle containsString:@"Saved"]) {
        self.customTitle = @"savedjobs";
    }
    
    UIImage *icon = nil;
    if(self.customIconName) {
        icon = [UIImage imageNamed:self.customIconName];
    } else {
        icon = [UIImage imageNamed:[NSString stringWithFormat:@"%@Icon",self.customTitle]];
    }
    
    self.titleImage.image = icon;
    self.titleLabel.font = FONT_DEFAULT(20);
    
    float lableHeight = self.titleLabel.font.pointSize;
    [self.titleLabel mas_makeConstraints:^(MASConstraintMaker *make) {
        make.height.mas_equalTo(lableHeight);
    }];
    self.titleLabel.textColor = WHITE;

    
}

-(void)configureCustomIconName:(NSString *)name
{
    self.customIconName = name;
}

-(void)configureCustomTitle:(NSString *)title
{
    self.customTitle = title;
}

@end
